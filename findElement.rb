def element_exists?(elements, element_to_be_found)
	found = false
	elements.each do |element|
		found = true if element === element_to_be_found 
	end
	found
end

puts element_exists?([1,2,3,4], 7)