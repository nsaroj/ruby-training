class NumbersInRange

  def initialize(start_range, end_range)
    @start_range = start_range
    @end_range = end_range  
  end

  def display
    (@start_range..@end_range).each do |i|
      puts i
    end
  end
end

one_to_hundred = NumbersInRange.new(1, 100)
one_to_hundred.display()